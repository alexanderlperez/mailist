describe('Basic app functionality', function () {

    describe('ListCtrl', function () {  
        var $controller;
        var $scope;
        var ListCtrl;
        var QueueList;

        beforeEach(function () {
            module('mailist');

            inject(function (_$controller_, $rootScope, _QueueList_) {
                $controller = _$controller_;
                $scope = $rootScope;
                QueueList = _QueueList_;
            });

            ListCtrl = $controller('ListCtrl', { $scope: $scope });
        });

        it('has a ListCtrl', function () {
            expect(ListCtrl).toBeDefined();
        });
    });

    describe('TextCtrl', function () {
        var $controller;
        var $scope;
        var TextCtrl;

        beforeEach(function () {
            module('mailist');

            inject(function (_$controller_, $rootScope) {
                $controller = _$controller_;
                $scope = $rootScope;
            })

            TextCtrl = $controller('TextCtrl', { $scope: $scope });
        })

        it('has a TextCtrl', function () {
            expect(TextCtrl).toBeDefined();
        })

    })

    describe('QueueList', function () {
        var QueueList;

        var testList = {
            fields: [ 
                ["email", "name", "company", "phone"]
            ],
            entries: [
                ["blah@email.com", "George Stephanopolos", "Google", "123-456-7890"],
                ["blah1@email.com", "Tony Soprano", "Yahoo", "123-456-7890"]
            ],
        };

        beforeEach(function () {
            module('mailist');

            inject(function (_QueueList_) {
                QueueList = _QueueList_;
            })
        })

        it('has a QueueList service', function () { 
            expect(QueueList).toBeDefined();
        })

        it('is initialized with an empty list', function () {
            var list = QueueList.listData.queueList;

            expect(list.entries).toBeDefined();
            expect(list.entries.length).toBe(0);

            expect(list.fields).toBeDefined();
            expect(list.fields.length).toBe(0);
        })
    })
})
