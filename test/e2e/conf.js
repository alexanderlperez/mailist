exports.config = {
    framework: 'jasmine2',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['mailist-spec.js'],
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            // --allow-file-access-from-files - allow XHR from file://
            args: ['allow-file-access-from-files']
        }
    },
    onPrepare: function () {
        browser.resetUrl = 'file://';
    }
}
