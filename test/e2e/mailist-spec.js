var path = require('path');
var fs = require('fs');
var readline = require('readline');
var _ = require('underscore');
var striptags = require('striptags');

var PUBLICROOT = __dirname + '../../../';

describe('Mailist Initialization', function () {
    /*

        Feature: Mailist user completes an email backlog using a template

        As a mailist user
        I want to send a list of templated emails to various recipients
        So that I can contact a group of individuals with very similar requests quickly and easily

    */

    beforeAll(function () {
        browser.get(PUBLICROOT + 'public/index.html');
    })

    /*

       Scenario 1: A user initially starts the app 

       Given there are no emails
           And there is no template
           And there is no cached data
       When the user starts the app
       Then the list of emails should be empty
           And the text area should be empty

     */

    describe('Initial load', function () {
        it('has an empty queue list', function () {
            element
                .all(by.repeater('item in vm.list'))
                .then(function (items) {
                    expect(items.length).toBe(0);
                })
        })

        it('has an empty textarea', function () {
            var textarea = element(by.model('vm.activeListingHtml')).element(by.css('[contenteditable]'));
            expect(textarea.getText()).toBe('');
        })
    })


    /*  

        Scenario 2: A user loads a list and generates a queue

        Given there are no emails
            And there is no template 
            And there is no cached data
        When the user loads a csv of data
        Then the queue list should be populated 
            And there should be relevent field headers
            And the first listing should be active

     */

    describe('Creating a list after initial load', function () {
        var file = path.resolve(__dirname, 'test1.csv');
        var headings;
        var firstLine;

        beforeAll(function (done) {
            element(by.css('input[type="file"]')).sendKeys(file);

            var count = 0;
            readline
                .createInterface({ input: fs.createReadStream(file) })
                .on('line', function (line) {
                    if (count == 0) {
                        headings = line;
                    }
                    if (count == 1) {
                        firstLine = line;
                        done();
                    }

                    count++;
                })
        })

        it('creates a table of the csv with the correct headings', function () {
            expect(element.all(by.css('.queue-list tbody tr')).count()).toBeGreaterThan(0);
            expect(element.all(by.css('.queue-list thead td')).count()).toEqual(headings.split(',').length);
        })
    })

    /* 

        Scenario 3: A user clicks on a list item and makes it active

        Given a CSV file as been loaded
            And and a Queue has been created 
            And there is no template
        When the user clicks a listing
        Then that listing should become highlighted
            And the composer area should be filled with the listing's data

     */

    describe('Creating a list after initial load', function () {
        var file = path.resolve(__dirname, 'test1.csv');
        var second;
        var secondLine;

        beforeAll(function (done) {
            element(by.css('input[type="file"]')).sendKeys(file);

            second = element
                .all(by.css('.queue-list tbody tr'))
                .get(1)
                .click();

            // get the second line
            var counter = 0;
            
            readline
                .createInterface({ input: fs.createReadStream(file) })
                .on('line', function (line) {
                    if (counter == 2) {
                        secondLine = line;
                        this.close();
                        done();
                    }

                    counter++;
                })
        })

        it('highlights the listing when clicked', function(done) {
            second.then(function () {
                var hasActiveOnSecond = second.getAttribute('class')
                    .then(function (attr) {
                        return attr.indexOf('active') > -1;
                    })

                var hasActiveOnNotSecond = element
                    .all(by.css('.queue-list tbody tr'))
                    .filter(function (elem, index) {
                        return index > 1;
                    })
                    .reduce(function (accum, elem) {
                        return elem.getAttribute('class').then(function (attr) {
                            return attr.indexOf('active') > -1 ? accum + 1 : null;
                        })
                    }, 0)
                    .then(function (accum) {
                        return accum > 1;
                    })

                expect(hasActiveOnSecond).toBe(true);
                expect(hasActiveOnNotSecond).toBe(false);
                
                done();
            });
        });

        it('fills out the composer area with listing data', function () {
            var emailField = element(by.css('.composer .email'));
            var textarea = element(by.model('vm.activeListingHtml')).element(by.css('[contenteditable]'));

            expect(emailField.getAttribute('value')).toBe(secondLine.split(',')[0]);
            expect(textarea.getText()).toBe(secondLine.split(',').slice(1).join().replace(/,/g, ' '))
        })
    });

    /* 

        Scenario 4: A user clicks on a list item and makes it active

        Given a CSV file as been loaded
            And and a Queue has been created 
            And there is no template
        When the user uploads a template
        Then the composer should use the template to render the textarea text

     */

    describe('Creating a list after initial load', function () {
        var list = path.resolve(__dirname, 'test1.csv');
        var template = path.resolve(__dirname, 'template.txt');
        var fieldsLine;
        var second;
        var secondLine;

        beforeAll(function (done) {
            element(by.css('.template input[type="file"]')).sendKeys(template);

            setTimeout(function () {
                element(by.css('.list input[type="file"]')).sendKeys(list);

                // get the second line
                var counter = 0;

                readline
                .createInterface({ input: fs.createReadStream(list) })
                .on('line', function (line) {
                    if (counter == 0) {
                        fieldsLine = line;
                    }
                    if (counter == 2) {
                        secondLine = line;
                        this.close();
                        done();
                    }

                    counter++;
                })
            }, 1000)

        })

        it('fills out the composer area with listing data', function () {
            var emailField = element(by.css('.composer .email'));
            var textarea = element(by.model('vm.activeListingHtml')).element(by.css('[contenteditable]'));
            var fields = {};
            var testEntry = secondLine.split(',');
            
            fieldsLine.split(',').forEach(function (field, i) {
                fields[field] = testEntry[i];
            });

            var templateText = fs.readFileSync(template, "utf8");
            var compiled = _.template(templateText);
            activeListingHtml = compiled({ data: fields })

            element
                .all(by.css('.queue-list tbody tr'))
                .get(0)
                .click()
            element
                .all(by.css('.queue-list tbody tr'))
                .get(1)
                .click()

            expect(emailField.getAttribute('value')).toBe(secondLine.split(',')[0]);
            expect(textarea.getText()).toBe(striptags(activeListingHtml).replace(/\n$/, ""))
        })
    });
})
