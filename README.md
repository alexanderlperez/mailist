Live: http://alexanderlperez.com/projects/mailist/public/


A BDD/TDD testbed for Angular and Protractor.  Helps with dealing with many emails in a todo list. Related article: http://alexanderlperez.com/index.php/2015/11/02/recent-work-bdd-with-angularjs-and-protractor/

![Screenshot](http://i.imgur.com/IzXqCHx.png)