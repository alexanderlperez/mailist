(function () {
    "set strict";

    // Controllers

    // handles create
    function ListCtrl(QueueList) {
        var vm = this;
  
        vm.queueList = QueueList;
        vm.updateActiveListing = function (index) {
            QueueList.listData.activeListing = QueueList.listData.queueList.entries[index];
            QueueList.listData.activeListingIndex = index;
        };
    }

    // handles getting active listings and running them through the template processor
    function TextCtrl(_, $scope, QueueList, Template) {
        var vm = this;

        vm.queueList = QueueList;
        vm.Template = Template;
        vm.activeListingHtml;

        $scope.$watch(function () {
            return QueueList.listData.activeListing;
        }, function (newVal) {
            if (newVal && !Template.templateData.currentTemplate){ 
                vm.activeListingHtml = newVal.slice(1).join(' ');
            }
            if (newVal && Template.templateData.currentTemplate){ 
                var fields = {};
                
                QueueList.listData.queueList.fields.forEach(function (field, i) {
                    fields[field] = QueueList.listData.activeListing[i];
                });

                var compiled = _.template(Template.templateData.currentTemplate);
                vm.activeListingHtml = compiled({ data: fields })
                console.log('html', _.template(Template.templateData.currentTemplate)({ data: fields }));
            }
        })
    }

    function QueueList() {
        var listData = {
            activeListing: undefined,
            activeListingIndex: 0,
            queueList: {
                fields: [],
                entries: [],
            }
        };

        return {
            listData: listData,
        };
    }

    function Template() {
        var templateData = {
            currentTemplate: undefined
        };

        return {
            templateData: templateData,
        };
    }

    function fileRead(QueueList) {
        function link(scope, element, attr, ctrl) {
            element.on('change', function (changeEvent) {
                var reader = new FileReader();

                reader.onload = function (loadEvent) {
                    var csv = CSV.parse(loadEvent.target.result);

                    QueueList.listData.queueList = {
                        fields: csv[0],
                        entries: csv.slice(1),
                    };
                    QueueList.listData.activeListingIndex = undefined;

                    scope.$digest();
                };

                reader.readAsText(changeEvent.target.files[0]);
            });
        }

        return {
            restrict: 'E',
            template: '<input type="file" />',
            link: link
        }
    }

    function templateRead(Template) {
        function link(scope, element, attr, ctrl) {
            element.on('change', function (changeEvent) {
                var reader = new FileReader();

                reader.onload = function (loadEvent) {
                    Template.templateData.currentTemplate = loadEvent.target.result;
                    scope.$digest();
                };

                reader.readAsText(changeEvent.target.files[0]);
            });
        }

        return {
            restrict: 'E',
            template: '<input type="file" />',
            link: link
        }
    }

    angular
        .module('mailist', ['textAngular', 'ui.sortable'])
        .constant('_', window._)
        .controller('ListCtrl', ListCtrl)
        .controller('TextCtrl', TextCtrl)
        .factory('QueueList', QueueList)
        .factory('Template', Template)
        .directive('fileRead', fileRead)
        .directive('templateRead', templateRead)

})();
